import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { Message } from '../message/entities/message.entity';
import { AddParticipantsDto } from '../../entrypoints/kafka/chat-events/dto/add-participants.dto';
import { CreateChatDto } from '../../entrypoints/kafka/chat-events/dto/create-chat.dto';
import { UpdateChatDto } from '../../entrypoints/kafka/chat-events/dto/update-chat.dto';
import { Chat } from './entities/chat.entity';
import { FindAllChatsDto } from '../../entrypoints/kafka/chat-events/dto/find-all-chats.dto';

@Injectable()
export class ChatService {
  constructor(@InjectModel(Chat.name) private readonly chatModel: Model<Chat>) {}

  async createChat(createChatDto: CreateChatDto) {
    const newChat = await this.chatModel.create(createChatDto);

    return newChat;
  }

  async findAllChats({ limit = 10, skip = 0 }: FindAllChatsDto = {}) {
    const allChats = await this.chatModel
      .find()
      .populate({ path: 'messages', model: Message.name })
      .limit(limit)
      .skip(skip)
      .exec();

    return allChats;
  }

  async findChat(id: string) {
    const chat = await this.chatModel
      .findById(id, { messages: { $slice: -50 } })
      .populate({
        path: 'messages',
        model: Message.name,
      })
      .exec();

    return chat;
  }

  async updateChat({ id, data }: UpdateChatDto) {
    return this.chatModel.findByIdAndUpdate(id, data, { new: true }).exec();
  }

  async pushNewMessage(id: string, messageId: Types.ObjectId) {
    const chat = await this.chatModel
      .findOneAndUpdate(
        { _id: id },
        {
          $push: { messages: messageId },
          $inc: { unreadMessagesCount: 1 },
        },
        { new: true },
      )
      .exec();

    if (!chat) {
      throw new BadRequestException('Chat not found');
    }

    return chat;
  }

  async addParticipants({ id, participants }: AddParticipantsDto) {
    const updatedChat = await this.chatModel
      .findByIdAndUpdate(id, { $push: { participants: { $each: participants } } }, { new: true })
      .exec();

    return updatedChat;
  }

  async removeChat(id: string) {
    const chat = await this.chatModel.findOneAndDelete({ _id: id }).exec();

    if (!chat) {
      throw new BadRequestException('Chat does not exist');
    }

    return chat;
  }

  async removeMessageFromChat(chatId: string, messageId: string) {
    await this.chatModel.updateOne(
      { _id: chatId },
      { $pull: { messages: new Types.ObjectId(messageId) } },
    );
  }
}

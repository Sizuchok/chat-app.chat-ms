import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { Message } from '../../message/entities/message.entity';
import { Participant } from './participant.entity';

@Schema({ timestamps: true })
export class Chat extends Document {
  @Prop({ required: true })
  name: string;

  @Prop({
    required: true,
    type: [Participant],
    validate: [
      (value: Array<Participant>) => value.length >= 2,
      'A chat must have at least two participants.',
    ],
  })
  participants: Participant[];

  @Prop({
    required: true,
    type: [{ type: Types.ObjectId, ref: Message.name }],
    default: [],
  })
  messages: Types.ObjectId[];

  @Prop({ required: true, default: 0 })
  unreadMessagesCount: number;
}

export const ChatSchema = SchemaFactory.createForClass(Chat);

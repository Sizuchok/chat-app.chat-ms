import { Prop } from '@nestjs/mongoose';
import { Role } from '../../../entrypoints/kafka/chat-events/enum/role.enum';

export class Participant {
  @Prop({ required: true })
  userId: string;

  @Prop({ required: true })
  name: string;

  @Prop({ required: true })
  nickname: string;

  @Prop({ required: true })
  email: string;

  @Prop()
  surname?: string;

  @Prop({ required: true, default: Role.Member })
  role: Role;
}

import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Message, MessageSchema } from './entities/message.entity';
import { MessageService } from './message.service';

@Module({
  providers: [MessageService],
  exports: [MessageService],
  imports: [MongooseModule.forFeature([{ name: Message.name, schema: MessageSchema }])],
})
export class MessageModule {}

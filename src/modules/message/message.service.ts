import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { Message } from '../message/entities/message.entity';
import { CreateMessageDto } from '../../entrypoints/ws/message-gateway/dto/create-message.dto';

@Injectable()
export class MessageService {
  constructor(@InjectModel(Message.name) private readonly messageModel: Model<Message>) {}

  async createMessage(createMessageDto: Omit<CreateMessageDto, 'chatId'>) {
    const message = await this.messageModel.create(createMessageDto);

    return message;
  }

  async removeMessage(id: string) {
    await this.messageModel.deleteOne({ _id: id });
  }

  async removeManyMessages(messageIds: Types.ObjectId[]) {
    await this.messageModel.deleteMany({ _id: { $in: messageIds } });
  }
}

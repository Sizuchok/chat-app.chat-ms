import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { Participant } from '../../chat/entities/participant.entity';

@Schema({ timestamps: true })
export class Message extends Document<Types.ObjectId> {
  @Prop({ required: true })
  content: string;

  @Prop({
    required: true,
    type: Participant,
  })
  author: Participant;

  @Prop({ required: true, default: false })
  isRead: boolean;
}

export const MessageSchema = SchemaFactory.createForClass(Message);

import { Module } from '@nestjs/common';
import { ChatModule } from '../chat/chat.module';
import { MessageModule } from '../message/message.module';
import { ChatMessageService } from './chat-message.service';

@Module({
  providers: [ChatMessageService],
  exports: [ChatMessageService],
  imports: [ChatModule, MessageModule],
})
export class ChatMessageModule {}

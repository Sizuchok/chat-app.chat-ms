import { Inject, Injectable } from '@nestjs/common';
import { ClientKafka } from '@nestjs/microservices';
import { Cron, CronExpression } from '@nestjs/schedule';
import { NOTIFICATIONS_MS_KEY } from '../../common/const/ms-keys.const';
import { ChatService } from '../chat/chat.service';
import { CHAT } from '../../entrypoints/kafka/chat-events/const/chat-events.const';
import { CreateMessageDto } from '../../entrypoints/ws/message-gateway/dto/create-message.dto';
import { RemoveMessageDto } from '../../entrypoints/ws/message-gateway/dto/remove-message.dto';
import { MessageService } from '../message/message.service';
import { MIN_NUMBER_OF_UNREAD_MSGS } from './chat-message.const';

@Injectable()
export class ChatMessageService {
  constructor(
    private readonly messageService: MessageService,
    private readonly chatService: ChatService,
    @Inject(NOTIFICATIONS_MS_KEY)
    private readonly notificationsClient: ClientKafka,
  ) {}

  async createMessage({ chatId, ...createMessageDto }: CreateMessageDto) {
    const message = await this.messageService.createMessage(createMessageDto);

    await this.chatService.pushNewMessage(chatId, message._id);

    return message;
  }

  @Cron(CronExpression.EVERY_WEEK)
  private async sendUnreadMessagesEmails() {
    const chats = await this.chatService.findAllChats();

    const emailsData = chats
      .map((chat) => {
        if (chat.unreadMessagesCount < MIN_NUMBER_OF_UNREAD_MSGS) {
          return null;
        }

        const emails = chat.participants.map((participant) => participant.email);

        return {
          chatName: chat.name,
          numberOfMessages: chat.unreadMessagesCount,
          emails,
        };
      })
      .filter(Boolean);

    emailsData.forEach((emailData) => {
      this.notificationsClient.emit(CHAT.NEW_UNREAD_MESSAGES, emailData);
    });
  }

  async removeMessage({ chatId, messageId }: RemoveMessageDto) {
    await this.messageService.removeMessage(messageId);

    await this.chatService.removeMessageFromChat(chatId, messageId);
  }

  async removeChat(id: string) {
    const chat = await this.chatService.removeChat(id);

    await this.messageService.removeManyMessages(chat.messages);
  }
}

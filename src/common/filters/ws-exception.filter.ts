import { ArgumentsHost, Catch, HttpException } from '@nestjs/common';
import { BaseWsExceptionFilter } from '@nestjs/websockets';
import { Socket } from 'socket.io';

@Catch()
export class WsExceptionFilter extends BaseWsExceptionFilter {
  catch(exception: unknown, host: ArgumentsHost) {
    const wsContext = host.switchToWs();
    const client: Socket = wsContext.getClient<Socket>();

    if (!(exception instanceof HttpException)) {
      return this.handleError(client, exception);
    }

    const status = exception.getStatus();
    const response = exception.getResponse() as Record<string, unknown> | string;

    const errorResponse = {
      status,
      message: typeof response === 'string' ? response : response.message,
    };

    client.emit('exception', errorResponse);
  }
}

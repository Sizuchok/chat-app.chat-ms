import { Global, Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { NOTIFICATIONS_MS_KEY } from '../const/ms-keys.const';
import {
  NOTIFICATIONS_MS_CLIENT_ID,
  NOTIFICATIONS_MS_CONSUMER_ID,
} from '../const/kafka/kafka-ms-config.const';
import { ConfigService } from '@nestjs/config';

@Global()
@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: NOTIFICATIONS_MS_KEY,
        useFactory: (configService: ConfigService) => {
          return {
            transport: Transport.KAFKA,
            options: {
              client: {
                brokers: [configService.get('KAFKA_BROKER_URL') as string],
                clientId: NOTIFICATIONS_MS_CLIENT_ID,
              },
              consumer: { groupId: NOTIFICATIONS_MS_CONSUMER_ID },
            },
          };
        },
        inject: [ConfigService],
      },
    ]),
  ],
  exports: [ClientsModule],
})
export class MsClientsModule {}

import { Type } from 'class-transformer';
import { IsMongoId, IsNotEmpty, IsString, ValidateNested } from 'class-validator';
import { CreateParticipantDto } from '../../../kafka/chat-events/dto/create-participant.dto';

export class CreateMessageDto {
  @IsNotEmpty()
  @IsString()
  content: string;

  @IsNotEmpty()
  @ValidateNested()
  @Type(() => CreateParticipantDto)
  author: CreateParticipantDto;

  @IsMongoId()
  chatId: string;
}

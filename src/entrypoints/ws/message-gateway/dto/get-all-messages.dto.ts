import { IsMongoId } from 'class-validator';

export class GetAllMessagesDto {
  @IsMongoId()
  chatId: string;
}

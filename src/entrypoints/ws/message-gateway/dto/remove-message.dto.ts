import { IsMongoId } from 'class-validator';

export class RemoveMessageDto {
  @IsMongoId()
  chatId: string;

  @IsMongoId()
  messageId: string;
}

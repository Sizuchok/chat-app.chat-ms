import { UseFilters, UsePipes } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import {
  MessageBody,
  OnGatewayConnection,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { WsExceptionFilter } from '../../../common/filters/ws-exception.filter';
import { WsValidationPipe } from '../../../common/pipes/ws-validation.pipe';
import { ChatMessageService } from '../../../modules/chat-message/chat-message.service';
import { MESSAGE_WS } from '../../kafka/chat-events/const/message-ws-events.const';
import { CreateMessageDto } from './dto/create-message.dto';
import { RemoveMessageDto } from './dto/remove-message.dto';

@UseFilters(WsExceptionFilter)
@UsePipes(WsValidationPipe)
@WebSocketGateway({
  cors: {
    origin: '*',
  },
})
export class MessageGateway implements OnGatewayConnection {
  constructor(
    private readonly chatMessageService: ChatMessageService,
    private readonly jwtService: JwtService,
  ) {}

  @WebSocketServer()
  private server: Server;

  async handleConnection(client: Socket) {
    const token = client.handshake.query.token as string;

    try {
      await this.jwtService.verifyAsync(token);
    } catch (error) {
      client.disconnect();
    }
  }

  @SubscribeMessage(MESSAGE_WS.CREATE)
  async createMessage(@MessageBody() createMessageDto: CreateMessageDto) {
    const message = await this.chatMessageService.createMessage(createMessageDto);

    this.server.emit(MESSAGE_WS.NEW, message);

    return message;
  }

  @SubscribeMessage(MESSAGE_WS.REMOVE)
  removeMessage(@MessageBody() removeMessageDto: RemoveMessageDto) {
    return this.chatMessageService.removeMessage(removeMessageDto);
  }
}

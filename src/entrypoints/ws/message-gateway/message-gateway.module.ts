import { Module } from '@nestjs/common';
import { MessageGateway } from './message.gateway';
import { MessageModule } from '../../../modules/message/message.module';
import { ChatMessageModule } from '../../../modules/chat-message/chat-message.module';

@Module({
  providers: [MessageGateway],
  imports: [MessageModule, ChatMessageModule],
})
export class MessageGatewayModule {}

export const MESSAGE_WS = {
  CREATE: 'message.create',
  NEW: 'mesage.broadcast-new',
  GET_ALL: 'message.get-all',
  REMOVE: 'message.remove-one',
} as const;

import { Type } from 'class-transformer';
import { ArrayMinSize, IsArray, IsMongoId, ValidateNested } from 'class-validator';
import { CreateParticipantDto } from './create-participant.dto';

export class AddParticipantsDto {
  @IsMongoId()
  id: string;

  @ArrayMinSize(1, { message: 'At least one participant should be specified.' })
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CreateParticipantDto)
  participants: CreateParticipantDto[];
}

import { OmitType, PartialType } from '@nestjs/mapped-types';
import { CreateChatDto } from './create-chat.dto';
import { IsMongoId } from 'class-validator';
import { Type } from 'class-transformer';

export class UpdateChatDataDto extends PartialType(OmitType(CreateChatDto, ['participants'])) {}

export class UpdateChatDto {
  @IsMongoId()
  id: string;

  @Type(() => UpdateChatDataDto)
  data: UpdateChatDataDto;
}

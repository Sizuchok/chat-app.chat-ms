import { ArrayMinSize, IsArray, IsNotEmpty, IsString, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { CreateParticipantDto } from './create-participant.dto';

export class CreateChatDto {
  @IsNotEmpty()
  @IsString()
  name: string;

  @ArrayMinSize(2, { message: 'A chat must have at least two participants.' })
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CreateParticipantDto)
  participants: CreateParticipantDto[];
}

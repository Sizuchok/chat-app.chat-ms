import { IsInt, IsOptional, IsPositive, Min } from 'class-validator';

export class FindAllChatsDto {
  @IsOptional()
  @IsPositive()
  @IsInt()
  limit?: number;

  @IsOptional()
  @IsInt()
  @Min(0)
  skip?: number;
}

import { IsEmail, IsEnum, IsMongoId, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { Role } from '../enum/role.enum';

export class CreateParticipantDto {
  @IsMongoId()
  userId: string;

  @IsNotEmpty()
  @IsString()
  name: string;

  @IsNotEmpty()
  @IsString()
  nickname: string;

  @IsEmail()
  email: string;

  @IsOptional()
  @IsNotEmpty()
  @IsString()
  surname?: string;

  @IsOptional()
  @IsEnum(Role)
  role?: Role;
}

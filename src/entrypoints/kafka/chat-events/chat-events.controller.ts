import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { ChatService } from '../../../modules/chat/chat.service';
import { CHAT } from './const/chat-events.const';
import { CreateChatDto } from './dto/create-chat.dto';
import { AddParticipantsDto } from './dto/add-participants.dto';
import { UpdateChatDto } from './dto/update-chat.dto';
import { ChatMessageService } from '../../../modules/chat-message/chat-message.service';
import { FindAllChatsDto } from './dto/find-all-chats.dto';

@Controller()
export class ChatEventsController {
  constructor(
    private readonly chatService: ChatService,
    private readonly chatMessageService: ChatMessageService,
  ) {}

  @MessagePattern(CHAT.CREATE)
  create(@Payload() createChatDto: CreateChatDto) {
    return this.chatService.createChat(createChatDto);
  }

  @MessagePattern(CHAT.GET_ALL)
  findAll(findAllChatsDto: FindAllChatsDto) {
    return this.chatService.findAllChats(findAllChatsDto);
  }

  @MessagePattern(CHAT.GET_ONE)
  findOne(@Payload() id: string) {
    return this.chatService.findChat(id);
  }

  @MessagePattern(CHAT.UPDATE_ONE)
  updateChat(@Payload() updateChatDto: UpdateChatDto) {
    return this.chatService.updateChat(updateChatDto);
  }

  @MessagePattern(CHAT.ADD_PARTICIPANTS)
  addParticipants(@Payload() addParticipantsDto: AddParticipantsDto) {
    return this.chatService.addParticipants(addParticipantsDto);
  }

  @MessagePattern(CHAT.REMOVE_ONE)
  async removeChat(@Payload() id: string) {
    await this.chatMessageService.removeChat(id);
  }
}

import { Module } from '@nestjs/common';
import { ChatEventsController } from './chat-events.controller';
import { ChatModule } from '../../../modules/chat/chat.module';
import { ChatMessageModule } from '../../../modules/chat-message/chat-message.module';

@Module({
  controllers: [ChatEventsController],
  imports: [ChatModule, ChatMessageModule],
})
export class ChatEventsModule {}

import { Serializer } from '@nestjs/microservices';

export interface KafkaRequest<T = any> {
  key: Buffer | string | null;
  value: T;
  headers: Record<string, any>;
}

export class KafkaRequestSerializer implements Serializer<any, KafkaRequest> {
  public serialize(value: any): KafkaRequest {
    const isKafkaMessage =
      value && typeof value === 'object' && ('key' in value || 'value' in value);

    if (!isKafkaMessage) {
      value = { value };
    }

    if ('value' in value) {
      value.value = this.encode(value.value);
    }

    if ('key' in value && value.key !== undefined) {
      value.key = this.encode(value.key);
    }

    if (!value.headers) {
      value.headers = {};
    }

    return value;
  }

  private encode(value: any): Buffer | string | null {
    if (value == null) {
      return null;
    } else if (typeof value === 'string' || Buffer.isBuffer(value)) {
      return value;
    } else if (this.isPrimitive(value)) {
      return value.toString();
    } else {
      return JSON.stringify(value);
    }
  }

  private isPrimitive(value: unknown): boolean {
    return value !== Object(value);
  }
}

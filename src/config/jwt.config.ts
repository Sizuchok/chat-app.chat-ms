import { JwtModuleOptions } from '@nestjs/jwt';

export const jwtConfig = () => {
  return {
    secret: process.env.JWT_SECRET_KEY,
  } as JwtModuleOptions;
};

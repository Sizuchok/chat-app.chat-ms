import { MongooseModuleFactoryOptions } from '@nestjs/mongoose';

export const mongodbConfig = () => {
  return {
    uri: process.env.MONGO_DB_URL,
    dbName: process.env.MONGO_DB_NAME,
  } as MongooseModuleFactoryOptions;
};

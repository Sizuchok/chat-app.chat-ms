import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { MongooseModule } from '@nestjs/mongoose';
import { ChatModule } from './modules/chat/chat.module';
import { MsClientsModule } from './common/modules/ms-clients.module';
import { jwtConfig } from './config/jwt.config';
import { mongodbConfig } from './config/mongodb.config';
import { MessageModule } from './modules/message/message.module';
import { ChatEventsModule } from './entrypoints/kafka/chat-events/chat-events.module';
import { MessageGatewayModule } from './entrypoints/ws/message-gateway/message-gateway.module';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    MongooseModule.forRootAsync({ useFactory: () => mongodbConfig() }),
    ChatModule,
    JwtModule.registerAsync({ global: true, useFactory: () => jwtConfig() }),
    MsClientsModule,
    MessageModule,
    ChatEventsModule,
    MessageGatewayModule,
    ScheduleModule.forRoot(),
  ],
})
export class AppModule {}

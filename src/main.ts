import { ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';
import { CHAT_MS_CLIENT_ID, CHAT_MS_CONSUMER_ID } from './common/const/kafka/kafka-ms-config.const';
import { KafkaRequestSerializer } from './config/kafka/kafka-serializer';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
    }),
  );

  const configService = app.get(ConfigService);
  const KAFKA_BROKER = configService.get('KAFKA_BROKER_URL') ?? '';

  app.connectMicroservice<MicroserviceOptions>({
    transport: Transport.KAFKA,
    options: {
      client: { brokers: [KAFKA_BROKER], clientId: CHAT_MS_CLIENT_ID },
      consumer: { groupId: CHAT_MS_CONSUMER_ID },
      serializer: new KafkaRequestSerializer(),
    },
  });

  await app.startAllMicroservices();
  await app.listen(3003);
}
bootstrap();
